'use strict';

function execute_computer_turn(){
    cards_to_guess_from = {weapons:[], rooms:[], suspects:[]};
    let arr = Array.prototype.slice.call(document.getElementById("weapons").getElementsByTagName("img"), 0 );
    arr.filter((x)=>{
        if(!x.hasAttribute("hidden")){
            hide_element(x.id);
            return x;
        }
    }).forEach((x)=>{
        cards_to_guess_from.weapons.push(x.id);
    });
    arr = Array.prototype.slice.call(document.getElementById("suspects").getElementsByTagName("img"), 0 );
    arr.filter((x)=>{
        if(!x.hasAttribute("hidden")){
            hide_element(x.id);
            return x;
        }
    }).forEach((x)=>{
        cards_to_guess_from.suspects.push(x.id);
    });
    arr = Array.prototype.slice.call(document.getElementById("rooms").getElementsByTagName("span"), 0 );
    arr.filter((x)=>{
        if(!x.hasAttribute("hidden")){
            hide_element(x.id);
            return x;
        }
    }).forEach((x)=>{
        cards_to_guess_from.rooms.push(x.id);
    });

    setTimeout(()=>{generate_guess()}, 2000);
}

function generate_guess(){
    if(correctly_guessed.weapon === ""){
        let rand_weapons = Math.floor(Math.random() * cards_to_guess_from.weapons.length);
        current_guess.weapon = cards_to_guess_from.weapons[rand_weapons];
    }else{
        current_guess.weapon = correctly_guessed.weapon;
    }
    if(correctly_guessed.room === ""){
        let rand_rooms = Math.floor(Math.random() * cards_to_guess_from.rooms.length);
        current_guess.room = cards_to_guess_from.rooms[rand_rooms];
    }else{
        current_guess.room = correctly_guessed.room;
    }
    if(correctly_guessed.suspect === ""){
        let rand_suspect = Math.floor(Math.random() * cards_to_guess_from.suspects.length);
        current_guess.suspect = cards_to_guess_from.suspects[rand_suspect];
    }else{
        current_guess.suspect = correctly_guessed.suspect;
    }

    fix_correctly_guessed();
    show_element(current_guess.suspect);
    show_element(current_guess.weapon);
    show_element(current_guess.room);
    document.getElementById(current_guess.room).appendChild(document.getElementById(current_guess.weapon));
    document.getElementById(current_guess.room).appendChild(document.getElementById(current_guess.suspect));
    setTimeout(function () {
        computer_guesses.push(current_guess);
        if(compare_with_incident()){
            record.innerHTML += "Computer guessed correctly.";
            record.innerHTML += "Murder was done by " + current_guess.suspect + " in " +
                current_guess.room + " using " + current_guess.weapon;
            record.innerHTML += ". Game has been won by computer. <br><br>";
            reset_guess();
            game_status = 0;
            // this part of the code to generate current date in UTC format is taken from
            // https://stackoverflow.com/questions/948532/how-do-you-convert-a-javascript-date-to-utc
            let now = new Date();
            let now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            end_game(user_name + " vs Computer on " + now_utc +". Won by computer <br><br>");
        }else{
            reset_guess();
            record.innerHTML += user_name+"'s turn: ";
            turn++;
            show_user_cards();
            turn_.innerHTML = user_name + "'s turn";
        }
    }, 3000);

}

function fix_correctly_guessed(){
    if(incident.weapon === current_guess.weapon){
        correctly_guessed.weapon = current_guess.weapon;
    }
    if(incident.room === current_guess.room){
        correctly_guessed.room = current_guess.room;
    }
    if(incident.suspect === current_guess.suspect){
        correctly_guessed.suspect = current_guess.suspect;
    }
}